def do_stuff(num=0):
    try:
        if num is not None:
            return int(num) + 5
        else:
            return "please enter a number"
    except ValueError as err:
        raise err
