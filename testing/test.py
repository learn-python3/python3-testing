import unittest
import main


class TestMain(unittest.TestCase):
    def setUp(self):
        # This runs before each function.
        pass

    def tearDown(self):
        # Runs after each function
        pass

    def test_do_stuff(self):
        """Should work with a number"""
        test_param = 10
        result = main.do_stuff(test_param)
        self.assertEqual(result, 15)

    def test_do_stuff2(self):
        """Should return a ValueError when entering a string"""
        test_param = "invalid-string"
        with self.assertRaises(ValueError):
            main.do_stuff(test_param)

    def test_do_stuff3(self):
        """Should return `please enter a number` when entering None"""
        test_param = None
        result = main.do_stuff(test_param)
        self.assertEqual(result, "please enter a number")

    def test_do_stuff4(self):
        """Should return a ValueError when entering empty string"""
        test_param = ""
        with self.assertRaises(ValueError):
            main.do_stuff(test_param)


if __name__ == "__main__":
    unittest.main()
