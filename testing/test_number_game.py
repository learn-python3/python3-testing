import unittest
from unittest.mock import patch
import number_game


class TestGuessinggame(unittest.TestCase):
    @patch("builtins.input", return_value=1)
    def test_handle_user_input_with_number(self, input):
        minValue = 1
        maxValue = 10
        guess = number_game.handle_user_input(minValue, maxValue)
        self.assertEqual(guess, 1)

    @patch("builtins.input", return_value="1")
    def test_handle_user_input_with_numeric_string(self, input):
        minValue = 1
        maxValue = 10
        guess = number_game.handle_user_input(minValue, maxValue)
        self.assertEqual(guess, 1)

    @patch("builtins.input", return_value="invalid-string")
    def test_handle_user_input_with_invalid_string(self, input):
        minValue = 1
        maxValue = 10
        with self.assertRaises(ValueError):
            guess = number_game.handle_user_input(minValue, maxValue)


if __name__ == "__main__":
    unittest.main()
