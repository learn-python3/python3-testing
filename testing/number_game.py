import sys
import random


def handle_user_input(minValue, maxValue):
    try:
        guess = int(input("Guess the integer: "))
        return guess
    except:
        print(
            f"Not an integer. Please use only integers between {minValue} and {maxValue}."
        )
        raise ValueError
    finally:
        print("Try again")


if __name__ == "__main__":

    if len(sys.argv) != 3:
        sys.exit("Please run the file with `randomgame.py minValue maxValue")

    _, minValue, maxValue = sys.argv

    try:
        number = random.randint(int(minValue), int(maxValue))
    except:
        sys.exit("`minValue` and `maxValue` must be integers! Exit.")

    guess = None

    while guess != number:
        try:
            handle_user_input(minValue, maxValue)
        except ValueError:
            continue

    else:
        print(f"Correct: the number was {number}. You win.")
